### Commands to compile and run:

1. run 'make' in your terminal in the root (ensure you have C++ installed ideally in a linux environment), this compiles the code and generates the object files and links them
2. run ./main in the terminal, this runs the executable acheived from the previous step

The report for this project is included in the pdf 'DSA2Writeup'