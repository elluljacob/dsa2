#include "stats.ih"

double Statistics::mean(vector<int> const &data) 
{
    if (data.empty()) return 0.0;
    return accumulate(data.begin(), data.end(), 0.0) / data.size();
}