#ifndef STATISTICS_HH
#define STATISTICS_HH

#include <vector>

class Statistics {
public:
    std::vector<int> steps;
    std::vector<int> operations;

    void addStepCount(int count);
    void addOperationCount(int count);

    double mean(std::vector<int> const &data);
    double stdDev(std::vector<int> const &data, double mean);
    double median(std::vector<int> data);
    void displayStatistics();
    void clearStats();
};

#endif