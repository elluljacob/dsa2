#include "stats.ih"

double Statistics::median(vector<int> data) 
{
    if (data.empty()) return 0.0;
    size_t size = data.size();
    sort(data.begin(), data.end());
    if (size % 2 == 0)
        return (data[size / 2 - 1] + data[size / 2]) / 2.0;
    else 
        return data[size / 2];
}