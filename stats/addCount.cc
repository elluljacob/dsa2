#include "stats.ih"

void Statistics::addStepCount(int count)
{
    steps.push_back(count);
}

void Statistics::addOperationCount(int count) 
{
    operations.push_back(count);
}