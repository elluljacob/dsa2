#include "stats.ih"

double Statistics::stdDev(vector<int> const &data, double mean) 
{
    if (data.size() <= 1) return 0.0;
    double variance = 0.0;
    for (int value : data) 
        variance += (value - mean) * (value - mean);
    
    variance /= data.size();
    return sqrt(variance);
}