#include "stats.ih"

void Statistics::clearStats() 
{
    steps.clear();
    operations.clear();
}