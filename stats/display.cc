#include "stats.ih"

void Statistics::displayStatistics() 
{
    double meanSteps = mean(steps);
    double stdDevSteps = stdDev(steps, meanSteps);
    double medianSteps = median(steps);
    double meanRotations = mean(operations);
    double stdDevRotations = stdDev(operations, meanRotations);
    double medianRotations = median(operations);

    cout << "Steps - Min: " << *min_element(steps.begin(), steps.end())
                << ", Max: " << *max_element(steps.begin(), steps.end())
                << ", Mean: " << meanSteps
                << ", StdDev: " << stdDevSteps
                << ", Median: " << medianSteps << '\n';

    cout << "Rotations/Promotions - Min: " << *min_element(operations.begin(), operations.end())
                << ", Max: " << *max_element(operations.begin(), operations.end())
                << ", Mean: " << meanRotations
                << ", StdDev: " << stdDevRotations
                << ", Median: " << medianRotations << "\n\n";
}