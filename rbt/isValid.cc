#include "rbt.hh"

bool RedBlackTree::isRed(RBTNode *node) const 
{
    return node != nullptr && node->colour == RED;
}

bool RedBlackTree::validateNodeColours(RBTNode *node) const 
{
    if (node == nullptr) return true; // NIL nodes are black by definition

    // If a node is red, its children must be black (property 4)
    if (isRed(node)) 
        if (isRed(node->left) || isRed(node->right)) 
            return false;

    return validateNodeColours(node->left) && validateNodeColours(node->right);
}

bool RedBlackTree::validateBlackHeight(RBTNode *node, int &blackCount, int pathBlackCount = 0) const 
{
    if (node == nullptr) {
        // Leaf node reached, compare black count along this path to the expected count
        if (blackCount == -1) {
            blackCount = pathBlackCount; // Initialize black count on the first leaf encounter
            return true;
        } else {
            return blackCount == pathBlackCount; // Compare with black count on subsequent leaf encounters
        }
    }

    int nextPathBlackCount = pathBlackCount + (node->colour == BLACK ? 1 : 0);

    // Recursively validate left and right children
    return validateBlackHeight(node->left, blackCount, nextPathBlackCount) &&
           validateBlackHeight(node->right, blackCount, nextPathBlackCount);
}

bool RedBlackTree::isValid() const 
{
    // Check root is black
    if (isRed(root)) return false;

    // Validate node colours recursively
    if (!validateNodeColours(root)) return false;

    // Validate black height
    int blackCount = -1;
    return validateBlackHeight(root, blackCount);
}
