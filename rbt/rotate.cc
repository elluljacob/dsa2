#include "rbt.ih"

void RedBlackTree::rotateLeft(RBTNode *&ptr) 
{
    RBTNode *rightChild = ptr->right;
    ptr->right = rightChild->left;

    if (ptr->right != nullptr) 
        ptr->right->parent = ptr;

    rightChild->parent = ptr->parent;

    if (ptr->parent == nullptr) 
        root = rightChild;
    else if (ptr == ptr->parent->left) 
        ptr->parent->left = rightChild;
    else 
        ptr->parent->right = rightChild;

    rightChild->left = ptr;
    ptr->parent = rightChild;
}

void RedBlackTree::rotateRight(RBTNode *&ptr) 
{
    RBTNode *leftChild = ptr->left;
    ptr->left = leftChild->right;

    if (ptr->left != nullptr) 
        ptr->left->parent = ptr;

    leftChild->parent = ptr->parent;

    if (ptr->parent == nullptr) 
        root = leftChild;
    else if (ptr == ptr->parent->left) 
        ptr->parent->left = leftChild;
    else 
        ptr->parent->right = leftChild;
    

    leftChild->right = ptr;
    ptr->parent = leftChild;
}