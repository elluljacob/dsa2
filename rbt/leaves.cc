#include "rbt.ih"

size_t RedBlackTree::countLeaves(RBTNode const *node) const 
{
    if (node == nullptr) 
        return 0; // An empty tree has no leaves
    else if (node->left == nullptr && node->right == nullptr) 
        return 1; // A node with no children is a leaf
    else 
        return countLeaves(node->left) + countLeaves(node->right);
}
