#include "rbt.ih"

void RedBlackTree::fixViolation(RBTNode *&ptr) 
{
    RBTNode *parentPtr = nullptr;
    RBTNode *grandParentPtr = nullptr;

    while ((ptr != root) && (ptr->colour != BLACK) && (ptr->parent->colour == RED)) 
    {
        parentPtr = ptr->parent;
        grandParentPtr = ptr->parent->parent;

        /* Case : A
            * Parent of ptr is left child of Grand-parent of ptr */
        if (parentPtr == grandParentPtr->left) 
        {
            RBTNode *unclePtr = grandParentPtr->right;

            /* Case : 1
                * The uncle of ptr is also red
                * Only Recolouring required */
            if (unclePtr != nullptr && unclePtr->colour == RED) 
            {
                grandParentPtr->colour = RED;
                parentPtr->colour = BLACK;
                unclePtr->colour = BLACK;
                ptr = grandParentPtr;
            } 
            else 
            {
                /* Case : 2
                    * ptr is right child of its parent
                    * Left-rotation required */
                if (ptr == parentPtr->right) 
                {
                    rotateLeft(parentPtr);
                    ptr = parentPtr;
                    parentPtr = ptr->parent;
                }

                /* Case : 3
                    * ptr is left child of its parent
                    * Right-rotation required */
                rotateRight(grandParentPtr);
                swap(parentPtr->colour, grandParentPtr->colour);
                ptr = parentPtr;
            }
        } 
        else 
        {
            /* Case : B
                * Parent of ptr is right child of Grand-parent of ptr */
            RBTNode *unclePtr = grandParentPtr->left;

            /* Case : 1
                * The uncle of ptr is also red
                * Only Recolouring required */
            if ((unclePtr != nullptr) && (unclePtr->colour == RED)) 
            {
                grandParentPtr->colour = RED;
                parentPtr->colour = BLACK;
                unclePtr->colour = BLACK;
                ptr = grandParentPtr;
            } 
            else 
            {
                /* Case : 2
                    * ptr is left child of its parent
                    * Right-rotation required */
                if (ptr == parentPtr->left) 
                {
                    rotateRight(parentPtr);
                    ptr = parentPtr;
                    parentPtr = ptr->parent;
                }

                /* Case : 3
                    * ptr is right child of its parent
                    * Left-rotation required */
                rotateLeft(grandParentPtr);
                swap(parentPtr->colour, grandParentPtr->colour);
                ptr = parentPtr;
            }
        }
    }

    root->colour = BLACK;
}