#include "rbt.ih"

size_t RedBlackTree::calculateHeight(RBTNode const *node) const 
{
    if (node == nullptr) 
        return 0; // Height of empty tree is 0
    else 
    {
        size_t leftHeight = calculateHeight(node->left);
        size_t rightHeight = calculateHeight(node->right);
        return 1 + max(leftHeight, rightHeight); // Add 1 to account for the current node
    }
}

