#ifndef RBT_HH
#define RBT_HH

#include <iostream>
#include <vector>

enum NodeColour 
{
    RED,
    BLACK
};

struct RBTNode 
{
    int data;
    NodeColour colour;
    RBTNode *left, *right, *parent;

    RBTNode(int data);
};

class RedBlackTree 
{
protected:
    RBTNode *root;

    void rotateLeft(RBTNode *&ptr);
    void rotateRight(RBTNode *&ptr);
    void fixViolation(RBTNode *&ptr);
    bool insert(RBTNode *&root, RBTNode *&ptr);
    bool isRed(RBTNode *node) const; 
    bool validateNodeColours(RBTNode *node) const; 
    bool validateBlackHeight(RBTNode *node, int &blackCount, int const pathBlackCount) const; 

public:
    RedBlackTree();
    void display() const;
    void insert(int const &data);
    size_t calculateHeight(RBTNode const *RBTNode) const; 
    void inOrderDisplay(RBTNode *root) const;
    size_t countLeaves(RBTNode const *RBTNode) const; 
    bool isValid() const; 
    
};

#endif