#include "rbt.ih"

void RedBlackTree::inOrderDisplay(RBTNode *root) const 
{
    if (root != nullptr) 
    {
        inOrderDisplay(root->left); // Visit left subtree
        cout << root->data << " (" << (root->colour == RED ? "R" : "B") << ") ";
        inOrderDisplay(root->right); // Visit right subtree
    }
}

void RedBlackTree::display() const 
{
    inOrderDisplay(root);
    cout << '\n';
}