#include "rbt.ih"

bool RedBlackTree::insert(RBTNode *&root, RBTNode *&ptr) 
{
    if (root == nullptr) 
    {
        root = ptr;
        return true; // Insertion successful
    }

    if (ptr->data < root->data) 
    {
        if (!insert(root->left, ptr)) 
            return false; // Duplicate found in left subtree
        
        root->left->parent = root;
    } 
    else if (ptr->data > root->data) 
    {
        if (!insert(root->right, ptr)) 
            return false; // Duplicate found in right subtree
        root->right->parent = root;
    } 
    else  // Duplicate value not allowed.
        return false; // Insertion failed due to duplicate
    return true; // Insertion successful
}

void RedBlackTree::insert(int const &data)
{
    RBTNode *ptr = new RBTNode(data);

    // Insert in the binary search tree fashion.
    bool inserted = insert(root, ptr);

    // Fix the red black tree violations if any, only if the node was actually inserted.
    if (inserted) 
        fixViolation(ptr);
    else 
        // Insertion failed (duplicate), delete the node to prevent memory leak.
        delete ptr;
}