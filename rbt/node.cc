#include "rbt.ih"

RBTNode::RBTNode(int data)
: 
    data(data), 
    colour(RED), 
    left(nullptr), 
    right(nullptr), 
    parent(nullptr) 
{}
