#ifndef AVL_STATS_HH
#define AVL_STATS_HH

#include "../avl/avl.hh"
#include "../stats/stats.hh"

class AVLStats : public AVL, public Statistics {
    Node *insert(Node *node, int key, int &steps, int &rotations);
public:
    AVLStats();

    void insert(int value) override; // Overriden to include statistics tracking
    void displayStatistics(); // Method to display statistics specific to AVL operations
};

#endif
