#include "avlStats.hh"

#include <iostream>
using namespace std;

AVLStats::AVLStats() 
: 
    AVL() 
{}

void AVLStats::insert(int value)
{
    int steps = 0; // Init step counter
    int rotations = 0; // .. rotation counter
    root = insert(root, value, steps, rotations);
    addStepCount(steps);
    addOperationCount(rotations); 
}

// Modified insert function that tracks steps and rotations
Node *AVLStats::insert(Node *node, int key, int &steps, int &rotations) 
{
    ++steps; // Increment steps for each node visit

    // Perform the normal BST insert
    if (node == nullptr) 
        return new Node(key);
    if (key < node->key)
        node->left = insert(node->left, key, steps, rotations);
    else if (key > node->key)
        node->right = insert(node->right, key, steps, rotations);
    else // Equal keys are not allowed in BST
        return node;

    // Update height and balance factor
    node->height = 1 + max(height(node->left), height(node->right));
    int balance = getBalanceFactor(node);

    // Perform rotations if necessary and increment rotation count accordingly
    // Left Left Case
    if (balance > 1 && key < node->left->key) 
    {
        ++rotations;
        node = rightRotate(node);
    }
    // Right Right Case
    else if (balance < -1 && key > node->right->key) 
    {
        ++rotations;
        node = leftRotate(node);
    }
    // Left Right Case
    else if (balance > 1 && key > node->left->key) 
    {
        ++rotations; // Counting LR as 1 rotation
        node->left = leftRotate(node->left);
        node = rightRotate(node);
    }
    // Right Left Case
    else if (balance < -1 && key < node->right->key) 
    {
        ++rotations;
        node->right = rightRotate(node->right);
        node = leftRotate(node);
    }

    return node; // Return the (possibly changed) node pointer
}

void AVLStats::displayStatistics() 
{
    cout << "AVL Tree Statistics:\n";
    cout << "Height: " << height(root) << '\n';
    cout << "Leaves: " << countLeaves(root) << '\n';
    Statistics::displayStatistics();
}