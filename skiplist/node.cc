#include "skiplist.ih"

SkipList::Node::Node(size_t level, int value) 
: 
    value(value) 
{
    forward = new Node*[level + 1];
    memset(forward, 0, sizeof(Node*) * (level + 1));
}

