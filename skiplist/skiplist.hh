#ifndef SKIP_LIST_HH
#define SKIP_LIST_HH

#include <cstddef>

class SkipList 
{
protected:
    struct Node 
    {
        int value;
        Node **forward;
        Node(size_t level, int value);
    };
    Node *header;
    size_t level;

    size_t randomLevel() const;
    float frand() const;

public:
    SkipList();

    virtual void insert(int value);
};

#endif