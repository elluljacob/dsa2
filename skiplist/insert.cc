#include "skiplist.ih"

void SkipList::insert(int value) 
{
    Node *current = header;
    Node *update[MAX_LEVEL + 1];
    memset(update, 0, sizeof(Node*) * (MAX_LEVEL + 1));

    for (int i = level; i >= 0; --i) 
    {
        while (current->forward[i] != nullptr && current->forward[i]->value < value) 
            current = current->forward[i];
        update[i] = current;
    }
    current = current->forward[0];

    if (current == nullptr || current->value != value) 
    {
        size_t lvl = randomLevel();
        if (lvl > level) 
        {
            for (size_t i = level + 1; i <= lvl; i++)
                update[i] = header;
            
            level = lvl;
        }

        Node* newNode = new Node(lvl, value);
        for (size_t i = 0; i <= lvl; ++i) 
        {
            newNode->forward[i] = update[i]->forward[i];
            update[i]->forward[i] = newNode;
        }
    }
}