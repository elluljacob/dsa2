#ifndef SHUFFLE_HH
#define SHUFFLE_HH

#include <vector>
#include <cstdlib> 
#include <ctime> 

void knuthShuffle(std::vector<size_t> &array);

#endif