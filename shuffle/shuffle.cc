#include "shuffle.hh"

using namespace std;

void knuthShuffle(vector<size_t> &array) 
{
    srand(static_cast<unsigned int>(time(nullptr))); // Seed the random number generator
    
    size_t n = array.size();
    for (size_t i = n - 1; i > 0; --i) 
    {
        size_t j = rand() % (i + 1); // Generate a random number in [0, i]
        swap(array[i], array[j]); // Swap array[i] with the element at the random index
    }
}