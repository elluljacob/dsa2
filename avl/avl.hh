#ifndef AVL_HH
#define AVL_HH

#include <cstddef>
#include <string>
struct Node 
{
    int key;
    Node *left, *right;
    int height;

    Node(int val);
};

class AVL 
{
public:
    AVL();

    virtual void insert(int value);// Public method to insert a value into the AVL tree
    void display(); // Public method to display the AVL tree
    void displayHeight(); // Public method to get the height of the AVL tree
    bool isValid(); // Public method to check if the AVL tree is valid
    void printTree();
    
protected:
    Node* root;

    Node *insert(Node *node, int value); // Recursive function to insert a value into the AVL tree
    size_t height(Node *node); // Utility function to get the height of the tree
    size_t countLeaves(Node *node); // .. leaves of the tree
    int getBalanceFactor(Node *N); // .. get balance factor of node N
    Node *rightRotate(Node *y);  // Right rotate utility
    Node *leftRotate(Node *x); // left ..

    void printTreeUtil(std::string const &prefix, Node const *node, bool isRight);
    void displayInternal(Node *root, std::string indent, bool last); 
    void inorder(Node *node);
    bool verifyAVLProperty(Node *node);
};

#endif