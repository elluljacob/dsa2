#include "avl.ih"

size_t AVL::countLeaves(Node *node) 
{
    if (node == nullptr) 
        return 0;
    
    if (node->left == nullptr && node->right == nullptr) 
        return 1; // This is a leaf node
    
    // Recursively count the leaves of both subtrees and sum them up
    return countLeaves(node->left) + countLeaves(node->right);
}