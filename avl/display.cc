#include "avl.ih"

void AVL::display() 
{
    inorder(root);
    cout << '\n'; 
}

void AVL::inorder(Node *root) 
{
    if (root != nullptr) 
    {
        inorder(root->left);
        cout << root->key << " ";
        inorder(root->right);
    }
}