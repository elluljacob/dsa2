#include "avl.ih"

Node::Node(int value) 
: 
    key(value), 
    left(nullptr), 
    right(nullptr), 
    height(1) 
{}