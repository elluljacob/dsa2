#include "avl.ih"

int AVL::getBalanceFactor(Node *N) 
{
    if (N == nullptr) return 0;
    return height(N->left) - height(N->right);
}