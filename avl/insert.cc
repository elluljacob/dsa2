#include "avl.ih"

void AVL::insert(int value) 
{
    root = insert(root, value);
}

Node *AVL::insert(Node *node, int key) 
{
    // 1. Perform the normal BST insert
    if (node == nullptr) return(new Node(key));

    if (key < node->key)
        node->left = insert(node->left, key);
    else if (key > node->key)
        node->right = insert(node->right, key);
    else // Equal keys are not allowed in our BST
        return node;

    // 2. Update height of the ancestor node
    node->height = 1 + max(height(node->left), height(node->right));

    // 3. Get balance factor of the ancestor node to check whether this node became unbalanced
    int balance = getBalanceFactor(node);

    // If this node becomes unbalanced, then there are 4 cases

    // Left Left Case
    if (balance > 1 && key < node->left->key)
        return rightRotate(node);

    // Right Right Case
    if (balance < -1 && key > node->right->key)
        return leftRotate(node);

    // Left Right Case
    if (balance > 1 && key > node->left->key) 
    {
        node->left = leftRotate(node->left);
        return rightRotate(node);
    }

    // Right Left Case
    if (balance < -1 && key < node->right->key) 
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }

    return node;
}