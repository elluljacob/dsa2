#include "avl.ih"

size_t AVL::height(Node *N) 
{
    if (N == nullptr) return 0;
    return N->height;
}

void AVL::displayHeight() 
{
    cout << "Height of the tree: " << height(root) << '\n';
}
void AVL::printTreeUtil(string const &prefix, Node const *node, bool isRight){
    if (node != nullptr) {
        cout << prefix;
        cout << (isRight ? "├──" : "└──");
        cout << node->key << '\n';

        // Enter the next tree level - left and right branch
        printTreeUtil(prefix + (isRight ? "│   " : "    "), node->right, true);
        printTreeUtil(prefix + (isRight ? "│   " : "    "), node->left, false);
    }
}

void AVL::printTree(){
    if (root == nullptr) {
        cout << "Tree is empty.\n";
        return;
    }
    cout << root->key << '\n';
    printTreeUtil("", root->right, true);
    printTreeUtil("", root->left , false);
}