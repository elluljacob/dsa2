#include "avl.ih"

bool AVL::isValid() 
{
    return verifyAVLProperty(root);
}

bool AVL::verifyAVLProperty(Node *node) 
{
    if (node == nullptr) return true;

    int leftHeight = height(node->left);
    int rightHeight = height(node->right);

    // Check the AVL property at the current node
    if (abs(leftHeight - rightHeight) > 1) return false;

    // Recursively verify the AVL property of the subtrees
    return verifyAVLProperty(node->left) && verifyAVLProperty(node->right);
}