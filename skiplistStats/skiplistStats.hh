#ifndef SKIPLISTSTATS_HH
#define SKIPLISTSTATS_HH

#include "../skiplist/skiplist.hh"
#include "../stats/stats.hh"

class SkipListStats : public SkipList, public Statistics {
public:
    SkipListStats();
    ~SkipListStats();

    void insert(int value) override; // Override to include statistics tracking
    void displayStatistics(); // Method to display statistics specific to Skip List operations
    
private:
    size_t promotions; // Keep track of promotions for each insert
};

#endif // SKIPLISTSTATS_HH

