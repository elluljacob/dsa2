#include "skiplistStats.hh"
#include <iostream>
#include <vector>
#include <cstring>
using namespace std;

#define MAX_LEVEL 12

SkipListStats::SkipListStats() 
: 
    SkipList(), 
    promotions(0) 
{}

SkipListStats::~SkipListStats() {}

void SkipListStats::insert(int value) 
{
    size_t steps = 0; // Initialize step counter for this insertion
    promotions = 0; // Reset promotions for this insertion
    
    Node *current = header;
    Node *update[MAX_LEVEL + 1];
    memset(update, 0, sizeof(Node*) * (MAX_LEVEL + 1));

    for (int i = level; i >= 0; --i) 
    {
        while (current->forward[i] != nullptr && current->forward[i]->value < value) 
        {
            current = current->forward[i];
            ++steps; // Count each hop as a step
        }
        update[i] = current;
    }
    current = current->forward[0];

    if (current == nullptr || current->value != value) 
    {
        size_t lvl = randomLevel();
        promotions += lvl + 1; // Count promotions, including level 0
        
        if (lvl > level) 
        {
            for (size_t i = level + 1; i <= lvl; ++i) 
                update[i] = header;
            level = lvl;
        }

        Node* newNode = new Node(lvl, value);
        for (size_t i = 0; i <= lvl; ++i) 
        {
            newNode->forward[i] = update[i]->forward[i];
            update[i]->forward[i] = newNode;
        }
    }
    addStepCount(steps);
    addOperationCount(promotions);
}

void SkipListStats::displayStatistics() 
{
    cout << "Skip List Statistics:" << '\n';
    cout << "Levels: " << level << '\n';
    Statistics::displayStatistics(); 
}