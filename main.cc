#include "main.ih"

int main() 
{
    vector<size_t> array(5000); 
    for (size_t i = 0; i < array.size(); ++i) 
        array[i] = i + 1; 
    
    knuthShuffle(array);

    AVLStats avl;
    RedBlackTreeStats rbt;
    SkipListStats skiplist;
    for (size_t i = 0; i < array.size(); ++i) 
    {
        avl.insert(array[i]);
        skiplist.insert(array[i]);
        rbt.insert(array[i]);
    }

    avl.clearStats();
    rbt.clearStats();
    skiplist.clearStats();

    vector<size_t> statsArray(1000); 
    for (size_t i = 0; i < statsArray.size(); ++i) 
        statsArray[i] = distr(gen);

    knuthShuffle(statsArray);
    
    for (size_t i = 0; i < statsArray.size(); ++i) 
    {
        avl.insert(statsArray[i]);
        rbt.insert(statsArray[i]);
        skiplist.insert(statsArray[i]);
    }
    avl.displayStatistics();
    rbt.displayStatistics();
    skiplist.displayStatistics();
}
