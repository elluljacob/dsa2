#ifndef RBT_STATS_HH
#define RBT_STATS_HH

#include "../rbt/rbt.hh"
#include "../stats/stats.hh"

class RedBlackTreeStats : public RedBlackTree, public Statistics {
public:
    RedBlackTreeStats();

    void insert(int const &data); // Override to include statistics tracking
    void displayStatistics(); // Method to display statistics specific to Red-Black Tree operations

private:
    RBTNode *insert(RBTNode *&root, RBTNode *pt, int &steps, int &rotations);
    void fixViolation(RBTNode *&root, RBTNode *&pt, int &rotations);
};


#endif