#include "rbtStats.hh"

#include <iostream>
using namespace std;

RedBlackTreeStats::RedBlackTreeStats() 
: 
    RedBlackTree() 
{}

// Override insert to include statistics tracking
void RedBlackTreeStats::insert(int const &data) 
{
    int steps = 0; // Track comparisons
    int rotations = 0; // Track rotations
    RBTNode *pt = new RBTNode(data);

    // Attempt to insert the node into the tree
    RBTNode *inserted = insert(root, pt, steps, rotations);

    if (inserted != nullptr) 
        fixViolation(root, pt, rotations); // If insertion was successful, fix any violations
    else
        delete pt; // If insertion was not successful (duplicate), delete the node

    // Add the collected statistics
    addStepCount(steps);
    addOperationCount(rotations);
}

RBTNode *RedBlackTreeStats::insert(RBTNode *&root, RBTNode *pt, int &steps, int &rotations) 
{
    
    if (root == nullptr) // Insert the node using the base class method, track comparisons and rotations
    {
        root = pt;
        return pt; // Insertion successful
    }

    ++steps; // Compare data for insertion
    if (pt->data < root->data) 
    {
        if (root->left == nullptr) 
        {
            root->left = pt;
            pt->parent = root;
            return pt;
        } 
        else 
            return insert(root->left, pt, steps, rotations);
    } 
    else if (pt->data > root->data) 
    {
        if (root->right == nullptr) 
        {
            root->right = pt;
            pt->parent = root;
            return pt;
        } else 
            return insert(root->right, pt, steps, rotations);
    }

    return nullptr; // Duplicate data, insertion failed
}

void RedBlackTreeStats::fixViolation(RBTNode *&root, RBTNode *&pt, int &rotations) 
{
    RBTNode *parent_pt = nullptr;
    RBTNode *grand_parent_pt = nullptr;

    while ((pt != root) && (pt->colour != BLACK) && (pt->parent->colour == RED)) 
    {
        parent_pt = pt->parent;
        grand_parent_pt = parent_pt->parent;

        if (parent_pt == grand_parent_pt->left) 
        {
            RBTNode *uncle_pt = grand_parent_pt->right;

            if (uncle_pt != nullptr && uncle_pt->colour == RED) 
            {
                grand_parent_pt->colour = RED;
                parent_pt->colour = BLACK;
                uncle_pt->colour = BLACK;
                pt = grand_parent_pt;
            } 
            else 
            {
                if (pt == parent_pt->right) 
                {
                    rotateLeft(parent_pt);
                    pt = parent_pt;
                    parent_pt = pt->parent;
                }

                rotateRight(grand_parent_pt);
                swap(parent_pt->colour, grand_parent_pt->colour);
                pt = parent_pt;
                ++rotations; 
            }
        } 
        else 
        {
            RBTNode *uncle_pt = grand_parent_pt->left;

            if ((uncle_pt != nullptr) && (uncle_pt->colour == RED)) 
            {
                grand_parent_pt->colour = RED;
                parent_pt->colour = BLACK;
                uncle_pt->colour = BLACK;
                pt = grand_parent_pt;
            } 
            else 
            {
                if (pt == parent_pt->left) 
                {
                    rotateRight(parent_pt);
                    pt = parent_pt;
                    parent_pt = pt->parent;
                }

                rotateLeft(grand_parent_pt);
                swap(parent_pt->colour, grand_parent_pt->colour);
                pt = parent_pt;
                ++rotations; 
            }
        }
    }

    root->colour = BLACK; // Ensure root is always black
}


void RedBlackTreeStats::displayStatistics() 
{
    cout << "Red-Black Tree Statistics:" << '\n';
    cout << "Height: " << calculateHeight(root) << '\n';
    cout << "Leaves: " << countLeaves(root) << '\n';
    Statistics::displayStatistics();
}

